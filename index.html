---
layout: default
---

<div class="home">
	<ol>
		<li class="rule">Accurate chat status</li>
		<p class="summary">Set your chat status to away when you are away. Set it to available when you are available. Respect the chat status of others.</p>
		<p>Chat status is a social cue. In the real world this may be an empty chair, a closed door, wearing headphones, or going to the break room. In the virtual world, your chat status serves the same function.</p>
		<ul>
			<li>Set your status to <img src="https://docs.microsoft.com/en-us/microsoftteams/media/presence_available.png"> Available if you are online and open to participate in conversations. 
				<ul>
					<li>If you see someone is available feel free to begin a conversation with them.</li>
				</ul>
			</li>
			<li>Set your status to <img src="https://docs.microsoft.com/en-us/microsoftteams/media/presence_busy.png"> Busy, or <img src="https://docs.microsoft.com/en-us/microsoftteams/media/presence_dnd.png"> Do not disturb if you are engaged in another activity and prefer not to be interrupted.
				<ul>
					<li>If you see someone's status is Busy, or Do not disturb save your message for later, this person prefers not to be interrupted right now. Your message could cause them to lose productivity through <a href="https://en.wikipedia.org/wiki/Cognitive_shifting">cognitive shifting.</a></li>
				</ul>
			</li>
			<li>Set your status to <img src="https://docs.microsoft.com/en-us/microsoftteams/media/presence_away.png"> Be right back, or <img src="https://docs.microsoft.com/en-us/microsoftteams/media/presence_away.png"> Appear away if you will be away from your computer for more than a moment.</li>
				<ul>
					<li>If you see someone's status is Be right back, or Appear away, they may not be at their computer right now. Act accordingly.</li>
				</ul>
			<li>If you are planning to be away from your computer, set your status to <img src="https://docs.microsoft.com/en-us/microsoftteams/media/presence_oof.png"> Out of Office or <img src="https://docs.microsoft.com/en-us/microsoftteams/media/presence_offline.png"> Offline.</li>
				<ul>
					<li>If you see someone's status is Out of Office or Offline, this person is not available for work related conversations. Messaging them now could cause unnecessary distraction, or in some cases even stress and anxiety. Save your message for later if you can.</li>
				</ul>
			<li>A status message can be a useful supplement to your status indicator. Consider adding a short message in addition to setting your status to convey additional details or information. This can help others to make informed decisions about how and when to begin conversations with you.</li>
		</ul>
		<p>As always, there will be exceptions. Understand the impact your message will have and be considerate before ignoring a chat status.</p>
		
		<li class="rule">Quick chat protocol (AKA "no hello")</li>
		<p class="summary">Establish a low-overhead way to begin quick conversations with others.</p>
		<p>In the physical workspace you can ask a question by just blurting it out. You can see your co-worker is available and simply say, "Got a minute? Let's talk about the tomato incident." If the person is available you just start talking about it.</p>
		<p>In the virtual workplace, this often turns into a complicated back-and-forth resulting in lost momentum. Consider the following exchange:</p>
		<ul>
			<li>Me: Hi!</li>
			<li>You: Hi.</li>
			<li>Me: Got a second?</li>
			<li>You: Yeah.</li>
			<li>Me: I have a question about the tomato incident.</li>
			<li>You: Ok</li>
			<li>Me: Can we chat by video?</li>
			<li>You: Sure, want me to start the call or should you?</li>
			<li>Me: I'll do it.</li>
			<li>Me: [initiates call]</li>
		</ul>
		<p>By the time we’ve negotiated when, how, and where we’re going to talk, we’ve lost momentum. Take all that overhead, multiply it by the number of casual conversations you have with coworkers, and it totals to a big waste of time. Such etiquette is useful when talking to someone you don’t know well. For a direct coworker, however, you need a protocol with low overhead—as low as peeking over a cubicle wall.</p>
		<p>You can negotiate some easy rules to avoid the extended conversations. For example, “Quick hangout? Tomato incident.” If available, your contact initiates the call. No formalities, no small talk, no confusion over who creates the session. Otherwise, the contact responds, “Not now,” and it is up to the requester to make an appointment.</p>
		<p>Compare the above example with this exchange:</p>
		<ul>
			<li>Me: Quick Chat? Tomato incident</li>
			<li>You: [initiates call]</li>
		</ul>
		<p>or...</p>
		<ul>
			<li>Me: Quick Chat? Tomato incident</li>
			<li>You: Busy right now, put something on my calendar please.</li>
		</ul>
		<p>When teams have prearranged to compress conversations that way, everyone benefits. In general, we want the overhead of starting a conversation to match the type of meeting. Important meetings with many people require planning, careful scheduling, and perhaps even a rehearsal. That’s an appropriate amount of overhead. Impromptu meetings should have minimal startup overhead.</p>
		<p>The quick chat protocol has become the virtual equivalent of social customs such as visiting someone’s office, striking up a quick conversation when you pass by someone in the hallway, or spotting someone at the water cooler and approaching with a question.</p>
		
		<li class="rule">If anyone is remote, we are all remote</li>
		<p class="summary">Meetings should be either 100% in-person, or 100% remote; no mixed meetings.</p>
		<p>Ever been in a conference room with a bunch of people plus one person participating by phone or videoconference? It never works well. The one remote participant can’t hear the conversation, can’t see what everyone else is seeing, and so on. He or she can’t authentically participate.</p>
		<p>This is more of a guideline than a hard rule. Mixed meetings can work if the space is set up correctly, but most importantly, everyone in the meeting must be aware of the remote participants. This requires a learned skill of being vigilant for clues that someone is having difficulty in participating and then taking corrective action. Everyone, not just the facilitator, needs to be mindful of this.</p>
	</ol>
</div>
